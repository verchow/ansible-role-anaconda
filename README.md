ansible-role-anaconda
========
This role installes the [Continuum Anaconda](https://www.continuum.io/anaconda-overview) distribution with all its packages (aka Anaconda, ~500 MB installer, ~3 GB installed) or just the conda package manager and Python (aka Miniconda, ~40 MB installer, ~400 MB installed).

You choose your desired version of Python and Anaconda by choosing a propper installer-file. Installer-files follow a schema: `(Ana|Mini)conda<Python-major-version>-<Distribution-version>-<Platform>.sh`
    
Examples:  
`Miniconda3-4.3.21-Linux-x86_64.sh` installs the conda package manager and Python-3.x from the distribution version 3.4.21 on 64-bit GNU/Linux.  
`Anaconda2-4.4.0-Linux-x86.sh` installs the full distribution version 4.4.0 with Python-2.x on 32-bit GNU/Linux.  

Online resource for Installer-files:
- Anaconda: https://repo.continuum.io/archive/
- Miniconda: https://repo.continuum.io/miniconda/

If you do not provide a installer file, you get a Miniconda installation with Python3 for x86_64 systems. See [role defaults](defaults/main.yml) for details.

It is possible to install Miniconda and run `conda install anaconda` afterwards to get the full Ananconda distribution.

Be aware that this role sets the path to the Anaconda distribution at the beginning of `$PATH` in a global way for bash/ksh/zsh/ash (e.g. `/etc/bash.bashrc.local`) by default. Keep that in mind if your system depends on a specific python version. Set `anaconda_set_path` to `False` and handle `$PATH` by your own if you don't like this.

The installer-file will reside in `anaconda_home_dir` to detect version changes. Despite the fact, that you can up/downgrade your Anaconda distribution with the conda package manager, a full reinstall (uninstall first, new install afterwards) will be performed if the installer-file or `anaconda_installer_uri` change. As long as you don't change `anaconda_installer_uri` and the installer-file, your Anaconda installation will not be changed.


Requirements
------------
This role should be almost OS-agnostic as long as you provide a suitable installer file and the target is a Unix-like system.
The target hosts need access to `anaconda_installer_uri` resource. But this can be a local resource on the target host with `file:///tmp/foo`. So a full offline installation can be done in a short playbook.


Role Variables
--------------
| parameter | required | default | choices | comments |
| :-------- | :------- | :------ | :-----: | :------- |
| anaconda_state | no | `present` | `present`, `absent` | Desired state of the Anaconda distribution. If `absent`, it tries to remove the Anaconda path from `$PATH`. |
| anaconda_home_dir | no | `/opt/anaconda` | | Path to Anaconda installation. |
| anaconda_installer_uri | no | see [defaults](defaults/main.yml) | | URI to the installer file. Use `https://` for remote or `file://` for local files on the target host. The target host requires access to that file. See above for online resources. |
| anaconda_installer_md5 | no | see [defaults](defaults/main.yml) | | The md5 checksum of the installer file. See above for online resources. |
| anaconda_installer_dir | no | `anaconda_home_dir` | | Destination folder for downloading the installer-file. If it's not `anaconda_home_dir` it will not be removed when `state = absent`. |
| anaconda_force_install | no | False | boolean | Run installer even if the installer-file is unchanged and Anaconda seems to be installed. |
| anaconda_set_path | no | True | boolean | Adds Anaconda's path to the beginning of `$PATH` for all bash/ksh/zsh/ash login shells. See above for details. |

Example playbooks
-----------------
    # installs Miniconda
    - host: python-dev-host
      roles:
        - ansible-role-anaconda
    
    # installes full Anaconda with Python 2.x into /usr/local/ but don't touch $PATH
    - host: python-dev-host
      roles:
        - role: ansible-role-anaconda
          anaconda_installer_uri: https://repo.continuum.io/archive/Anaconda2-4.4.0-Linux-x86_64.sh
          anaconda_installer_md5: d72add23bc937ccdfc7de4f47deff843
          anaconda_home_dir: /usr/local
          anaconda_set_path: False
          
    # removes anacona from its default home_dir and removes it from $PATH
    - host: python-dev-host
      roles:
        - { role: ansible-role-anaconda, anaconda_state: absent }


Dependencies
------------
bzip2 to extract installer-file.
